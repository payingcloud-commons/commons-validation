package cn.payingcloud.commons.validation.annotation;

import cn.payingcloud.commons.validation.constraint.IdCardValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * @author ZM.Wang
 */
@Target({METHOD, FIELD, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IdCardValidator.class)
@Documented
public @interface IdCard {
    String message() default "不是一个合法的身份证号";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
