package cn.payingcloud.commons.validation.constraint;

import cn.payingcloud.commons.validation.annotation.IdCard;

/**
 * @author ZM.Wang
 */
public class IdCardValidator extends RegularValidator<IdCard> {

    private final static String regEx = "^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X)$";

    @Override
    protected String getRegEx() {
        return regEx;
    }
}
