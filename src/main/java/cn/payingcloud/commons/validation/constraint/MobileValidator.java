package cn.payingcloud.commons.validation.constraint;

import cn.payingcloud.commons.validation.annotation.Mobile;

/**
 * @author ZM.Wang
 */
public class MobileValidator extends RegularValidator<Mobile> {

    private static final String regEx = "^1(3|4|5|7|8)[0-9]\\d{8}$";

    @Override
    protected String getRegEx() {
        return regEx;
    }
}
