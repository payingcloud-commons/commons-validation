package cn.payingcloud.commons.validation.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ZM.Wang
 */
public abstract class RegularValidator<T extends Annotation> implements ConstraintValidator<T, String> {

    @Override
    public void initialize(T t) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null || "".equals(value)) {
            return true;
        }
        Pattern pattern = Pattern.compile(getRegEx());
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }

    protected abstract String getRegEx();
}
