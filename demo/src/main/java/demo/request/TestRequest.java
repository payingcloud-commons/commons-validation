package demo.request;

import cn.payingcloud.commons.validation.annotation.IdCard;
import cn.payingcloud.commons.validation.annotation.Mobile;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author ZM.Wang
 */
public class TestRequest {

    @IdCard
    @ApiModelProperty("身份证")
    private String idCard;

    @Mobile
    @ApiModelProperty("手机号")
    private String phone;

    public String getIdCard() {
        return idCard;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "TestRequest{" +
                "idCard='" + idCard + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
