package demo.config;

import cn.payingcloud.commons.validation.annotation.IdCard;
import cn.payingcloud.commons.validation.annotation.Mobile;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author ZM.Wang
 */
@ConfigurationProperties(prefix = "test")
public class TestProperties {

    @IdCard
    private String idCard;

    @NotBlank
    @Mobile
    private String phone;

    public String getIdCard() {
        return idCard;
    }

    public TestProperties setIdCard(String idCard) {
        this.idCard = idCard;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public TestProperties setPhone(String phone) {
        this.phone = phone;
        return this;
    }
}
