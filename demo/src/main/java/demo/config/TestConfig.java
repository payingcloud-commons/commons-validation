package demo.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author ZM.Wang
 */
@Configuration
@EnableConfigurationProperties(TestProperties.class)
public class TestConfig {
}
