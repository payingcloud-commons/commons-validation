package demo.web;

import demo.request.TestRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author ZM.Wang
 */
@Api
@RestController
public class TestController {

    @ApiOperation("测试请求")
    @PostMapping("/test")
    public void test(@Valid @RequestBody TestRequest request) {
        System.err.println(request);
    }

}
