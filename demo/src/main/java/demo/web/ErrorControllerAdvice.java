package demo.web;

import cn.payingcloud.commons.rest.exceptionhandler.RestExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author ZM.Wang
 */
@RestControllerAdvice
class ErrorControllerAdvice extends RestExceptionHandler {
}
